[Home](../README.md)


# Table of Contents
- [ISW](#isw-소개)
- [Bian Scenario](#bain-scenario)
- [Service Domains](#service-domains)

---

# ISW 소개

- Personas
  ![persona](images/isw-personas.png)

- solution Designer  
  https://edu-k5-designer.apps.openshift-01.knowis.cloud/  
- Solution hub  
  https://edu-hub.apps.openshift-01.knowis.cloud/  
- Solution Envoy  
  https://education-dev.apps.openshift-01.knowis.cloud/solutions.html  

# Bain Scenario  


## Bian Architecture for enablement
- Architecture Overview  
  https://bian.org/servicelandscape-11-0-0/
- Bian Payment Scenario  
  https://bian.org/servicelandscape-11-0-0/views/view_53451.html  
- Cash Payment  
  https://bian.org/servicelandscape-11-0-0/views/view_53196.html  

## Handle Request for Cash Withdrawal from Savings Account
![bian-scenario](images/bian-scenario.png)  
![bian-ko](images/bian-ko.png)  

---

# Service Domains  
## Servicing Order  
Servicing Order 서비스 도메인은 여러 제품 및 서비스에 영향을 미칠 수 있고 처리 주기/단계가 포함될 수 있으며 관련 수수료/요금이 부과될 수 있는 요청의 처리를 처리합니다. 적절하게 선택하도록 정의된 여러 유형의 표준 '서비스 주문'이 있을 수 있습니다. 서비스 주문은 고객이 직접 시작하거나 공인된 제3자 서비스 제공업체가 고객을 대신하여 요청할 수 있습니다. 고객을 대신하여 제3자 서비스 제공업체가 요청하는 경우 관련 권한/의무가 있는지 확인하는 절차가 포함될 수 있습니다.  
- Service Domain Overview   
    https://bian.org/servicelandscape-11-0-0/views/view_53133.html  
- Service Domain Definition  
    https://bian.org/servicelandscape-11-0-0/object_28.html?object=36200  
- Servicing Order Control Record Diagram  
    https://bian.org/servicelandscape-11-0-0/views/view_41500.html  

## Payment Order  
Payment Order는 서비스 도메인 결제 실행에서 처리하는 실제 이체 메커니즘을 시작하기 전에 내부 은행 및 규정 준수 확인과 자금 이체 처리를 처리합니다. 여기에는 감시 목록 및 기타 규제 확인과 거래 상대방별 한도 및 결제 기본 설정 적용이 포함됩니다. 또한 은행과 다른 거래 상대방 간의 결제 상계 약정을 감독할 수도 있습니다.  
- Service Domain Overview   
    https://bian.org/servicelandscape-11-0-0/views/view_53490.html    
- Service Domain Definition   
    https://bian.org/servicelandscape-11-0-0/object_41.html?object=43702  
- Payment Order Control Record Diagram  
    https://bian.org/servicelandscape-11-0-0/views/view_44250.html  


## Party Lifecycle Management  
Party Lifecycle Managment 서비스 도메인은 고객 또는 파트너와 새로운 비즈니스 관계를 설정할 때 수행되는 초기 확인부터 관계 기간 동안 필요에 따라 수행되는 확인까지 은행과 당사자 관계의 상태를 추적합니다. 확인 및 유지 요건은 당사자 유형(예: 개인, 회사, 파트너) 및 관할권에 따라 달라집니다. 수표는 은행별, 법률 및 규제 고려 사항을 다루며 표준 일정에 따라 또는 특정 상황의 요청에 따라 업데이트될 수 있습니다.  
- Service Domain Overview   
    https://bian.org/servicelandscape-11-0-0/views/view_52609.html  
- Service Domain Definition   
    https://bian.org/servicelandscape-11-0-0/object_28.html?object=33839  
- Party Lifecycle Management Control Record Diagram  
    https://bian.org/servicelandscape-11-0-0/views/view_37011.html  

## Savings Account  
Savings Account는 일반 은행 당좌 예금 계좌와 많은 기능을 공유할 수 있지만(예: 당좌 주문, 자동 이체, 결제 및 입금 서비스), 일부 제약/제한이 있을 수 있습니다. 더 높은 이자 혜택을 상쇄하기 위해 관련 수수료/위약금 구조와 함께 인출 금액 및 횟수에 제한이 있을 수 있습니다. 다른 상품(당좌 계좌)과 연계된 스윕 메커니즘도 지원될 수 있습니다.  
- Service Domain Overview   
   https://bian.org/servicelandscape-11-0-0/views/view_53082.html  
- Service Domain Definition   
    https://bian.org/servicelandscape-11-0-0/object_40.html?object=31200  
- Savings Account Control Record Diagram   
    https://bian.org/servicelandscape-11-0-0/views/view_40672.html  

## Payment Execution  
Payment Execution은 채무자 계정에서 채권자 계정으로 송금하는 자금의 백엔드 처리를 처리합니다. 결제는 결제 실행에 지시되기 전에 고객/은행 계약에 따라 승인 및 유효성 검사가 완료되어야 합니다. 그런 다음 결제 실행은 채무자 및 채권자 계좌가 은행 내에 있는지 확인하고, 그렇지 않은 경우 이체를 완료하는 데 사용할 적절한 결제 메커니즘/채널을 선택합니다. 결제 실행은 양쪽(또는 양쪽 모두)의 교환이 성공적으로 완료되었는지 확인할 책임이 있습니다.  
- Service Domain Overview   
   https://bian.org/servicelandscape-11-0-0/views/view_52932.html  
- Service Domain Definition   
    https://bian.org/servicelandscape-11-0-0/object_29.html?object=39395  
- Savings Account Control Record Diagram   
    https://bian.org/servicelandscape-11-0-0/views/view_38760.html   

## Position Keeping 
관리 정보, 추적 및 조정 목적으로 금융 거래 로그를 유지합니다.  잔액/포지션을 유지하고, 이자를 계산하고, 거래 조정 활동을 지원하는 유틸리티를 제공할 수 있습니다. 금융 거래는 이후 회계 시스템에 게시됩니다.  
- Service Domain Overview   
   https://bian.org/servicelandscape-11-0-0/views/view_53448.html  
- Service Domain Definition   
    https://bian.org/servicelandscape-11-0-0/object_41.html?object=44631  
- Savings Account Control Record Diagram   
    https://bian.org/servicelandscape-11-0-0/views/view_44773.html   


## Internal Bank Account  
Internal Bank Account 서비스 도메인은 고객 계좌에 예약하지 않는 은행 세계(회계 세계가 아닌)에서 거래의 해당 부분을 예약하는 데 필요한 보류 계좌, 미러 계좌, 작업 계좌 등을 관리합니다. 일반적으로 업무 분리 또는 처리상의 이유로 최종 계좌에 금액을 즉시 예약할 수 없거나 노스트로 계좌와 같은 외부 계좌에 예약을 미러링하기 위한 경우에 해당합니다.
- Service Domain Overview   
   https://bian.org/servicelandscape-11-0-0/views/view_52845.html  
- Service Domain Definition   
    https://bian.org/servicelandscape-11-0-0/object_41.html?object=43316  
- Savings Account Control Record Diagram   
    https://bian.org/servicelandscape-11-0-0/views/view_36264.html   


# Working Process
## Servicing Order for Cash withdrawals
![Servicing Order](images/servicingorder.png)

## Payment Order
![Payment Order](images/payment-order.png)

## Payment Execution
![Payment Execution](images/payment-execution.png)

## Position Keeping
![Position Keeping](images/positionkeeping.png)